// export default () => <div>
    
// </div>
// import Link from 'next/link'
// import '../css/style.css'

export default () =>
  <div className='home'>
    <div className='top'>top</div>
    <div className='main'>main</div>
    <div className='bottom'>bottom</div>
    <style jsx>{`
      
*{
    padding: 0;
    margin: 0;
}
html,body{
    width: 100%;
    height: 100%;
}
.home{
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
}
.top{
    height: 100px;
    background-color: #eee;
}
.main{
    flex: 1;
    overflow-y: auto;
}
.bottom{
    height: 100px;
    background-color: #eee;
}
      }
    `}</style>
  </div>